import logging
import random

import numpy as np
import pytest

from generic_functions import create_test_output_dir, TEST_TEMP_INPUT_FILE_PATH, TEST_TEMP_OUTPUT_FILE_PATH, \
    run_program_with_file_anf_get_result

EXECUTABLE_FILE_PATH = "tests/my_set"
MINIMUM_VALUE = -10
MAXIMUM_VALUE = 10
ARRAY_SIZE = 30
ITERATION_NUMBER = 100
LOGGER = logging.getLogger(__name__)


class TestMySet:
    OUTPUT_LINE = "Your set looks like this:"
    SUBDIR_NAME = "my_set"

    @staticmethod
    def create_test_file(file_path: str, array: np.ndarray) -> str:
        with open(file_path, "w") as file:
            for value in array:
                if random.randint(0, 1) == 1:
                    file.write(f"{str(value)} ")
                else:
                    file.write(f"{str(value)} \n")
        return file_path

    @staticmethod
    def get_result_after_line(line: str, text: str) -> list:
        LOGGER.info("\n" + text + "\n")
        lines = text.split("\n")
        for index, lin in enumerate(lines):
            if lin == line:
                data = lines[index + 1]
                temp_list = []
                for i in data.split(" "):
                    if i:
                        temp_list.append(int(i))
                return temp_list
        raise Exception("This is not good")

    @pytest.mark.parametrize('execution_number', range(ITERATION_NUMBER))
    def test_my_set(self, execution_number):
        array = np.random.randint(MINIMUM_VALUE, MAXIMUM_VALUE, ARRAY_SIZE)
        expected_result = list(set(array))
        expected_result.sort()
        output_dir_path = create_test_output_dir(self.SUBDIR_NAME, str(execution_number))
        input_file = output_dir_path.joinpath(TEST_TEMP_INPUT_FILE_PATH).as_posix()
        output_file = output_dir_path.joinpath(TEST_TEMP_OUTPUT_FILE_PATH).as_posix()
        input_file_path = self.create_test_file(input_file, array)
        output = run_program_with_file_anf_get_result(EXECUTABLE_FILE_PATH, input_file_path, output_file)
        result_array = self.get_result_after_line(self.OUTPUT_LINE, output)
        result_array.sort()
        LOGGER.info(f"Expected set: \n {expected_result} \n Got set: \n {result_array} \n ")
        assert expected_result == result_array
