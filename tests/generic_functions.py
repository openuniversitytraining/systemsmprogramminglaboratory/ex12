import os
import subprocess
import time
from pathlib import Path


TEST_TEMP_INPUT_FILE_PATH = "input_file"
TEST_TEMP_OUTPUT_FILE_PATH = "output_file"


def run_program_with_file_anf_get_result(executable_file: str, input_file: str, output_file: str) -> str:
    process = subprocess.Popen(f"{executable_file} < {input_file} > {output_file}", shell=True)
    process.wait()
    data = ""
    with open(output_file, "r") as file:
        for line in file.readlines():
            data += line
    return data


def create_test_output_dir(dir_name: str, subdir_name: str) -> Path:
    output_path = Path("input_and_outputs_files")
    if not output_path.exists():
        os.mkdir(output_path)
    output_dir_path = output_path.joinpath(dir_name)
    test_output_dir_path = output_dir_path.joinpath(subdir_name)
    if not output_dir_path.exists():
        os.mkdir(output_dir_path)
    os.mkdir(test_output_dir_path)
    return test_output_dir_path





