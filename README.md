# ex12
In my_set dir, you can see the c file and executable files for this exercise.
In input_and_outputs_files you can see a lot of testings input and output files.
I also did write a few python files for testing, you can see them in my GitLab repo here:

https://gitlab.com/openuniversitytraining/systemsmprogramminglaboratory/ex12/-/pipelines/837509451

Also, I did all my work with CI/CD in GitLab. I have a gitlab-runner that runs on my ubuntu 16.04 vm and every time I push to the git my code will automatically build and test itself :)
You can see my CI/CD at this link:

https://gitlab.com/openuniversitytraining/systemsmprogramminglaboratory/ex12/-/pipelines

Just jump into the newest pipeline.