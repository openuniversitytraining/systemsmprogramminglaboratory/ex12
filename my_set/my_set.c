#include <stdio.h>
#include <stdlib.h>

#define BUFFER_INCREASE_SIZE 2
#define ENLARGE_SIZE(buffer)(buffer + BUFFER_INCREASE_SIZE)
#define true 1
#define false 0

/*
This struct represents a set, with a pointer to the values array, 
length of set and buffer of memory that allocates to the values.
*/
struct MySet {
    int* values;
    unsigned long length;
    unsigned long buffer;
};

/*
This function gets my_set struct and value_to_check in and then checks if the value_to_check is in my_set.values. 
The function returns 1 if value_to_check is in the set and 0 elsewhere.
*/
int check_if_value_in_set(struct MySet my_set, int value_to_check) {
    int index = 0;
    int is_in_set = false;
    for ( ; index < my_set.length; index++) {
        if (my_set.values[index] == value_to_check) {
            is_in_set = true;
            break;
        }
    }
    return is_in_set;
}

/*
This function gets the my_set struct, reads input from a user and updates
the my_set struct according to the entered data from a user.
For each input, we check if the input is not in my_set.values and if not we update the values.
We also check if the buffer is smaller than the length times the size of int, to update our buffer and to reallocate the memory.
*/
void get_set(struct MySet * my_set) {
    int input;
    int is_in_set;
    printf("Welcome to my_set C program!\n");
    printf("Please enter the values for the set, when you finish type in ctl+d.\n");
    /* While the user is entering values (indicated by the user not entering ctl+d) */
    while (EOF != scanf("%d", &input)) {
        /* Check if the input value is already in the set  */
        is_in_set = check_if_value_in_set(*my_set, input);
        if (is_in_set == false){
            /* Check if the current buffer of the set is sufficient. If not, resize the buffer by increasing its size */
            if (my_set->length + 1 > my_set->buffer) {
                my_set->values = realloc(my_set->values, sizeof(int) * ENLARGE_SIZE(my_set->buffer));
                if (!my_set->values) {
                    printf("Memory allocation failed!");
                    exit(1);
                }
                my_set->buffer += BUFFER_INCREASE_SIZE;
            }
            /* Add the input value to the end of the set and increment the length of the set */
            my_set->values[my_set->length] = input;
            my_set->length ++;
        }
        printf("You entered the number: %d \n", input);
    }
}

/*
This function gets my_set struct and runs over all its values and prints them.
*/
void print_set(struct MySet * my_set) {
    int index = 0;
    printf("Your set looks like this:\n");
    for ( ; index < my_set->length; index++) {
        printf("%d ", my_set->values[index]);
    }
    printf("\nGood Bye! Have a nice day :)\n");
}


int main() {
    int * values = (int*)calloc(BUFFER_INCREASE_SIZE,sizeof(int));
    if (!values) {
        printf("Memory allocation failed!");
        return 1;
    }
    struct MySet my_set = {values, 0, BUFFER_INCREASE_SIZE};
    get_set(&my_set);
    print_set(&my_set);
    free(my_set.values);
    return 0;
}

